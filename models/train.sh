export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
export LD_LIBRARY_PATH=LD_LIBRARY_PATH:/usr/local/cuda-9.0/lib64/
export CUDA_VISIBLE_DEVICES=0,1,2,3
CURRENT_DIR=$(pwd)
WORK_DIR="${CURRENT_DIR}/deeplab"
DATASET_DIR="datasets"


# Set up the working directories.
PQR_FOLDER="cityscapes"
EXP_FOLDER="exp/train_on_trainval_set"
INIT_FOLDER="${WORK_DIR}/${DATASET_DIR}/${PQR_FOLDER}/${EXP_FOLDER}/init_models"
TRAIN_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${PQR_FOLDER}/${EXP_FOLDER}/train"
EVAL_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${PQR_FOLDER}/${EXP_FOLDER}/eval"
DATASET="${WORK_DIR}/${DATASET_DIR}/${PQR_FOLDER}/tfrecord"


python "${WORK_DIR}"/train.py \
  --base_learning_rate=0.007 \
  --weight_decay=0.0001 \
  --num_clones=4 \
  --logtostderr \
  --train_split="train" \
  --model_variant="resnet_v1_50_beta" \
  --atrous_rates=6 \
  --atrous_rates=12 \
  --atrous_rates=18 \
  --output_stride=16 \
  --decoder_output_stride=4 \
  --train_crop_size=769 \
  --train_crop_size=769 \
  --train_batch_size=4 \
  --multi_grid=1 \
  --multi_grid=2 \
  --multi_grid=4 \
  --training_number_of_steps=90000 \
  --fine_tune_batch_norm=True\
  --dataset="cityscapes" \
  --tf_initial_checkpoint="${INIT_FOLDER}/resnet_v1_50/model.ckpt" \
  --train_logdir="${TRAIN_LOGDIR}" \
  --dataset_dir="${DATASET}"\
  --save_summaries_images=True \
  --initialize_last_layer=False \
  --last_layers_contain_logits_only=True

