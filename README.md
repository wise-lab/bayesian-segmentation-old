# Bayesian Uncertainty Quantification with Synthetic Data

This is a Tensorflow implementation for Bayesian uncertainty estimation in image segmentation. The code is  based on Google's deeplab repository (https://github.com/tensorflow/models/tree/master/research/deeplab). To estimate the Bayesian uncertainties, we modified the ResNet-50 architecture by inserting several dropout layers, which can be found at: ``bayesian_deeplab/research/deeplab/core/resnet_v1_beta.py``.

# Data preparation
- Generate the data by using the ProcSy tool and create the tfrecord by following the instruction in the ``bayesian_deeplab/research/deeplab/g3doc/cityscapes.md``

- Prepare the data and put it in the folder: ``bayesian_deeplab/research/deeplab/datasets/cityscapes/tfrecord``
<p align="center">
<img src="./figures/Data_preparation.png" width="500">
</p>
- Modify the number of images for training and validation in the file ``bayesian_deeplab/research/deeplab/datasets/segmentation_dataset.py
``
<p align="center">
<img src="./figures/segmentation_data.png" width="500">
</p>

# Model Training:

- To train the model, change the directory to ``bayesian_deeplab/research/`` and run:
``bash train.sh``.
- The model will be stored at: ``bayesian_deeplab/research/deeplab/datasets/cityscapes/exp/train_on_trainval_set/train``

# Model Testing:
- To evaluate the model (non-Bayesian), change the directory to ``bayesian_deeplab/research/`` and run:
``bash eval.sh``.

# Model Visualization
- To visualize the images, aleatoric, epistemic and predictive uncertainty, change the directory to ``bayesian_deeplab/research/`` and run:
``bash vis.sh``.
- To visualize the prediction, simply take argmax of the predictive uncertainty (2048x1024x19 -> 2048x1024).
- The visualization results will be stored at``bayesian_deeplab/research/deeplab/datasets/cityscapes/exp/train_on_trainval_set/train``

# To produce the weather experiment results in the paper

- Setup the weather dataset and run the visualization script (vis.sh)
- Copy (or cut) the vis folder into the ``Weather_experiments`` folder.
- Run the ``Entropy_MIOU_Calculation.ipynb`` notebook.
- Repeat for different factor intensity

# To produce the occlusion-depth experiment results in the paper (TODO: fix the occlusion map issue)
- The scripts in the Depth_Occlusion_Experiment folder only take into accounts occlusion maps containting one vehicle.
- The code for this is quite messy.

**Occlusion and depth map preprocessing:**
- For all the occlusion maps, choose only the ones containing 1 vehicle (delete the other map). Calculate the visibility amount by dividing the amount of visible pixels to the total amount of pixel for each instance.
- For all the depth maps:

  1. Extract carla-master.zip.
  2. Navigate to Util/ImageConverter/
  3. Follow the instruction and convert the bit depth map into float.
  4. Name the resulted folder as depth_images.
  5. Preprocess it so that it contains the same image as the occlusion map.

- Visualization: there are two files, 1 for accuracy ``plot_accuracy.ipynb`` and 1 for uncertainties (``Plot Uncertainty Map - Visualize.ipynb``).

  1. Navigate to this location:
  <p align="center">
  <img src="./figures/location_to_look.png" width="500">
  </p>
  change the line la = './rain00/AL_' to either: "path_to_MI_images/MI_" (for epistemic), "path_to_AL_images/AL_" (for aleatoric) or "path_to_PR_images/PR_" (for predictive entropy) and save the contour plot data in
  <p align="center">
  <img src="./figures/contour_save.png" width="500">
  </p>
  2. Run ``Plot everything.ipynb`` for visualize the end results.
