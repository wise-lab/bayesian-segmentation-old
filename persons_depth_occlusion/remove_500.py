#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
import glob
import numpy as np


# In[ ]:


dataset_path = '/home/c276huan/bayesian-segmentation/models/deeplab/datasets/cityscapes'
img_path = dataset_path + '/leftImg8bit/train'
annot_path = dataset_path + '/gtFine/train'


# In[ ]:


names = []
for cityname in os.listdir(img_path):
    for filename in os.listdir(img_path + '/' + cityname):
        names.append(filename.replace('_leftImg8bit.png', ''))
print(len(names))


# In[ ]:


names = np.random.choice(names, 475, replace=False)
print(names.shape)


# In[ ]:


for name in names:
    cityname = name.split('_')[0]
    annot_list = glob.glob(f'{annot_path}/{cityname}/{name}*', recursive=True)
    os.remove(f'{img_path}/{cityname}/{name}_leftImg8bit.png')
    for annot in annot_list:
        os.remove(annot)


# In[ ]:


names = []
for cityname in os.listdir(img_path):
    for filename in os.listdir(img_path + '/' + cityname):
        names.append(filename.replace('_leftImg8bit.png', ''))
print(len(names))

